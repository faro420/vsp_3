/**
 * 
 */
package test;

import accessor_one.ClassOneImplBase;
import accessor_one.SomeException112;
import mware_lib.NameService;
import mware_lib.ObjectBroker;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public class A1ClassOneImplTest {
	public static void main( String[] args ) {
		A1ClassOneImplProvider c1 = new A1ClassOneImplProvider();
		
		// Normal procedure described in Task to provide an Object vvvv
		ObjectBroker objBroker = ObjectBroker.init("141.22.27.103", 5555, false);
		NameService nameSvc = objBroker.getNameService();
		
		// Bind a new Object
		nameSvc.rebind( c1, "zumsel" );

		while (true) {}
		/*
		// Resolve an Object
		Object test = nameSvc.resolve("zumsel");
		ClassOneImplBase c1remote = ClassOneImplBase.narrowCast(test);
		
		// Invoke a remote Method
		try {
			String str = c1remote.methodOne(100, "test");
			System.out.println("Got >" + str + "< as return!");
		} 
		catch (SomeException112 e) { e.printStackTrace(); }
		
		objBroker.shutDown();
		*/
	}
}
