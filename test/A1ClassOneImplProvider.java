/**
 * 
 */
package test;

import accessor_one.ClassOneImplBase;
import accessor_one.SomeException112;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public class A1ClassOneImplProvider extends ClassOneImplBase{
	@Override
	public String methodOne(int param1, String param2) throws SomeException112 {
		System.out.println("I was INVOKED!");
		return "Erhalten: " + param1 + ", " + param2; 
	}
}
