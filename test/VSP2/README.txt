test3.jar - full test
 
java -cp .:test3.jar:Middleware.jar test3.Server2 <ns-host> <ns-port>
java -cp .:test3.jar test3.Client2 <ns-host> <ns-port>


test4.jar - single pack test

java -cp .:test4.jar test4.Server2  <ns-host> <ns-port>
java -cp .:test4.jar test4.Client2  <ns-host> <ns-port>


test5.jar - performance test

java -cp .:test5.jar test5.Server  <ns-host> <ns-port>
java -cp .:test5.jar test5.Client  <ns-host> <ns-port> [ <num-calls-method-1> [ <num-calls-method-2> ] ]

	(Default: 5000 calls each)


Pre in all cases: 
mware_lib and accessor_* (as appropriate) must be in this dir.