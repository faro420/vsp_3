/**
 * 
 */
package accessor_two;

import java.lang.reflect.Method;

import mware_lib.Invocation;
import mware_lib.Invocation_Return;
import mware_lib.ServiceInfo;
import mware_lib.Write;

/**
 * Project: VSP_3
 *
 * @author Andreas Mueller
 * @MatrNr 209 918 2
 *
 * 29.11.2016
 */
public class ClassOneImplRemote extends ClassOneImplBase{
	ServiceInfo srvInfo;
	
	public ClassOneImplRemote( ServiceInfo remoteObjData ) {
		srvInfo = remoteObjData;
	}
	
	@Override
	public double methodOne(String param1, double param2)
			throws SomeException112 {
		// Hacky way to get the current Method
		Method m = new Object(){}.getClass().getEnclosingMethod(); 
		
		// Create new Invocation object holding all necessary data
		Invocation<?, ?> invo =  new Invocation<>(m.getName(), srvInfo.getServiceName(), 
												  param1, true, 
												  param2, true);
		
		// Send Request and wait for response
		Object resObj = Write.writeObject(invo, srvInfo.getServerIP(), srvInfo.getServicePort()); // S
		
		// Cast Response to Invocation_Return object
		Invocation_Return<?> res = (Invocation_Return<?>)resObj;
		
		// Validate return types
		if( res.getValue().getClass().equals(double.class)){
			// Valid result
			return (double)res.getValue();
		}
		else if( res.getValue().getClass().equals(SomeException112.class) ) {
			// Exception was thrown
			Exception excp = (Exception) res.getValue();
			throw new SomeException112( excp.getMessage() );
		}
		else {
			// Should never happen...
			return 0;
		}
	}

	@Override
	public double methodTwo(String param1, double param2)
			throws SomeException112, SomeException604 {
		// Hacky way to get the current Method
		Method m = new Object(){}.getClass().getEnclosingMethod(); 
		
		// Create new Invocation object holding all necessary data
		Invocation<?, ?> invo =  new Invocation<>(m.getName(), srvInfo.getServiceName(), 
												  param1, true, 
												  param2, true);
		
		// Send Request and wait for response
		Object resObj = Write.writeObject(invo, srvInfo.getServerIP(), srvInfo.getServicePort()); // S
		
		// Cast Response to Invocation_Return object
		Invocation_Return<?> res = (Invocation_Return<?>)resObj;
		
		// Validate return types
		if( res.getValue().getClass().equals(int.class)){
			// Valid result
			return (double)res.getValue();
		}
		else if( res.getValue().getClass().equals(SomeException112.class) ) {
			// Exception was thrown
			Exception excp = (Exception) res.getValue();
			throw new SomeException112( excp.getMessage() );
		}
		else if( res.getValue().getClass().equals(SomeException604.class) ) {
			// Exception was thrown
			Exception excp = (Exception) res.getValue();
			throw new SomeException604( excp.getMessage() );
		}
		else {
			// Should never happen...
			return 0;
		}
	}

}
