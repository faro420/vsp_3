/**
 * 
 */
package accessor_two;

import mware_lib.ServiceInfo;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public abstract class ClassOneImplBase {
	public abstract double methodOne( String param1, double param2 )
		throws SomeException112;
	
	public abstract double methodTwo( String param1, double param2 )
		throws SomeException112, SomeException604;
	
	public static ClassOneImplBase narrowCast( Object rawObjectRef ){
		ServiceInfo remoteObjData = (ServiceInfo) rawObjectRef;
		
		return new ClassOneImplRemote( remoteObjData );
	}
}
