TEAM 10





# VSP_3
Was ich soweit ge�ndert habe:  
- Der ObjectBroker bekommt als Parameter die Adresse und den Port des DNS
- Der NameServiceImpl erg�nzt bei der Erzeugung das info ServiceInfo direkt um unsere Adresse und Port
- Der ObjectBroker hat nun auch eine Referenz auf den NameServiceImpl, damit er das Shutdown weiterreichen kann
- In der NameServiceImpl befindet sich nun ein Server attribut um die Services irgendwo zu verwalten
- Bei einem Rebind wird nun automatisch ein neuer Service in den Server eingetragen
- DNS_Server restruktoriert, sodass wir mehr kontrolle dr�ber haben
- DNS_Server von NameService erben lassen
- DNS_Server dispatch der Anfrage implementiert
- DNS_Server kann nun binden, rebinden, resolven und deleten

INFO:  
- DNS Server funktioniert vollst�ndig!
- NameServiceImpl funktioniert vollst�ndig!


-------------------
Notizen soweit

Neue Klassen: Server und Client sind im package actors. Mit der statischen Methode vom Client kann man auf ein Socket schreiben und eine Antwort bekommen. Beim Server habe ich versucht für mehrere Dienste jeweils einen Thread bereit zu stellen. Die Objektbroker Syntax von Schulz macht alles kaputt. Ich habe Objektbroker und NameService doch so angepasst, dass man den Syntax von der Aufgabenstellung verwenden kann. Man muss aber Server wieder anpassen.

Server, ServiceInfo und Service gehören zusammen.

XObject habe ich zu Invocation umbenannt, weil wir damit Funktionen aufrufen.

Todo:
- DNS Server in actors vervollständigen
- Server komplett nach Schulz - Syntax anpassen
- Die anderen 4 Message- Typen in message erstellen
- und die Methoden- Aufrufe mit if elseif else abfangen.

Ich gehe jetzt pennen..
