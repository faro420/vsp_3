package actors;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import mware_lib.NameService;
import mware_lib.ServiceInfo;
import mware_lib.Service_Abmeldung;
import mware_lib.Service_Anfrage;
import mware_lib.Service_Anmeldung;

public class DNS_Server extends NameService {
	private ArrayList<ServiceInfo> list = new ArrayList<ServiceInfo>(10);
	private int port;
	ServerSocket socket;
	
	public DNS_Server( int port ) {
		this.port = port;
		rundns();
	}
	
	private void rundns() {	
		try {
			socket = new ServerSocket(port);
		} catch (IOException e1) {
			e1.printStackTrace();
			stopdns();
		}
		
		System.out.println("DNS Server running!");
		
		while (true) { // Server Loop
			try {
				Socket skt = socket.accept();
				try {
					
					// hier kommt, was der Client haben möchte
					ObjectInputStream objectInput = new ObjectInputStream(skt.getInputStream());
					Object a = objectInput.readObject();
					
					// hier kommt die Antwort
					ObjectOutputStream objectOutput = new ObjectOutputStream(skt.getOutputStream());
					objectOutput.writeObject( dispatchRequest(a) );
					
				} catch (IOException e) {
					e.printStackTrace();
					stopdns();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					stopdns();
				}
			} catch (IOException e) {
				e.printStackTrace();
				stopdns();
			}
		} // ] Server Loop
	}
	
	private void stopdns() {
		try {
			socket.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean equals(Object other) {
		return false;
	}

	public Object dispatchRequest(Object obj) {
		String objClassName = obj.getClass().getSimpleName();
		
		Object responseObj = null;
		
		if( objClassName.equals(Service_Anmeldung.class.getSimpleName()) ){
			// Request is "register/rebind"
			Service_Anmeldung serReg = (Service_Anmeldung) obj;
			rebind( serReg.getInfo(), serReg.getInfo().getServiceName() );
			responseObj = (Object) serReg;
		}
		else if( objClassName.equals(Service_Anfrage.class.getSimpleName()) ){
			// Request is "get/resolve"
			Service_Anfrage serReq = (Service_Anfrage) obj;
			responseObj = (Object) new Service_Anfrage( (ServiceInfo) resolve( serReq.getInfo().getServiceName() ) );
		}
		else if( objClassName.equals(Service_Abmeldung.class.getSimpleName()) ){
			// Request is "delete/unbind"
			Service_Abmeldung serUnreg = (Service_Abmeldung) obj;
			responseObj = (Object) delete( serUnreg );
		}
		
		return responseObj;
	}
	
	public Service_Abmeldung delete( Service_Abmeldung servant ) {
		int idx = 0;
		for( ServiceInfo srvInfo : list ) {
			if( srvInfo.equals(servant.getInfo()) ){
				list.remove( idx );
				System.out.println("DNS: Removed Object \"" + servant.getInfo().getServiceName() + "\"");
				return servant;
			}
			
			idx++;
		}
		
		return new Service_Abmeldung( new ServiceInfo( "", "", 0));
	}
	
	@Override
	public void rebind(Object servant, String name) {
		Service_Anmeldung serReg = new Service_Anmeldung( (ServiceInfo) servant );
		
		for( ServiceInfo srvInfo : list ) {
			if( srvInfo.getServiceName().equals(name) ){
				// ServiceInfo with given name exists, replace Address and Port
				System.out.println("DNS: Rebinding Object \"" + name + "\"");
				srvInfo.setServerIP( serReg.getInfo().getServerIP() );
				srvInfo.setServicePort( serReg.getInfo().getServicePort() );
				return; // Done rebind
			}
		}
		
		// No ServiceInfo with given name exists, create new
		System.out.println("DNS: Binding Object \"" + name + "\"");
		list.add( serReg.getInfo() );
	}

	@Override
	public Object resolve(String name) {
		ServiceInfo foundObj = new ServiceInfo( "", "", 0 );
		
		System.out.println("DNS: Resolving " + name);
		
		for (ServiceInfo srvInfo : list) {
			if( srvInfo.getServiceName().equals(name) ) {
				foundObj = srvInfo;
				break;
			}
		}
		
		return foundObj;
	}
	
	
	
	
	public static void main(String[] args) {
		new DNS_Server(15010);
	}
}
