/**
 * 
 */
package accessor_one;

import mware_lib.ServiceInfo;

/**
 * Project: VS_3
 *
 * @author Andreas Mueller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public abstract class ClassOneImplBase {
	public abstract String methodOne( int param1, String param2 )
		throws SomeException112;
	
	public static ClassOneImplBase narrowCast( Object rawObjectRef ) {
		ServiceInfo remoteObjData = (ServiceInfo) rawObjectRef;
		
		return new ClassOneImplRemote( remoteObjData );
	}
}
