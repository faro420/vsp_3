/**
 * 
 */
package accessor_one;

import mware_lib.ServiceInfo;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public abstract class ClassTwoImplBase {
	public abstract int methodOne( double param1, String param2 )
		throws SomeException110;
	
	public abstract double methodTwo()
		throws SomeException112;
	
	public static ClassTwoImplBase narrowCast( Object rawObjectRef ){
		ServiceInfo remoteObjData = (ServiceInfo) rawObjectRef;
		
		return new ClassTwoImplRmote( remoteObjData );
	}
}
