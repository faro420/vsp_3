/**
 * 
 */
package accessor_one;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public class SomeException110 extends Exception {
	public SomeException110( String message ) { super( message ); }
}
