/**
 * 
 */
package mware_lib;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller (209 918 2)
 * @author Mir Farshid Baha (214 180 1)
 *
 *         29.11.2016
 */
public class ObjectBroker {
	private String serviceHost;
	private int listenPort;
	private boolean debug;
	private NameServiceImpl dns;

	private ObjectBroker(String serviceHost, int listenPort, boolean debug) {
		this.serviceHost = serviceHost;
		this.listenPort = listenPort;
		this.debug = debug;
		
		// Create new NameServiceImpl that is used onward and referenced by objectbroker to shutdown
		dns = new NameServiceImpl(serviceHost, listenPort);
	}

	public static ObjectBroker init(String serviceHost, int listenPort, boolean debug) {
		return new ObjectBroker(serviceHost, listenPort, debug);
	}

	public NameService getNameService() {
		return dns;
	}

	public void shutDown() {
		Server server = dns.getServer();
		if(server!=null){
			server.shutdown();
		}
	}
}
