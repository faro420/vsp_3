package mware_lib;

import java.io.Serializable;

public class Service_Anfrage  implements Serializable{

	private static final long serialVersionUID = 1L;
	private ServiceInfo info;
	
	public Service_Anfrage(ServiceInfo info) {
		this.info = info;
	}

	public ServiceInfo getInfo() {
		return info;
	}

}
