/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha
 * @MatrNr 2141801
 *
 * 29.11.2016
 */
package mware_lib;

import java.util.ArrayList;

public class Server {
	private ArrayList<Thread> services;
	private int numberOfServices = 0;

	public Server() {
		services = new ArrayList<Thread>(10);
	}

	public void addService(Object servant, ServiceInfo info) {
		if (numberOfServices < 10) {
			Service service = new Service( servant, info );
			new Thread(service).start();
			services.add(new Thread(service));
			numberOfServices++;
			
			System.out.println("New Service \"" + info.getServiceName() + "\" provided at Port " + info.getServicePort());
		}
	}

	public void shutdown() {
		int size = services.size();
		for (int i = 0; i < size; i++) {
			services.get(i).interrupt();
		}
	}
}
