package mware_lib;

import java.io.Serializable;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public class RemoteObjectData implements Serializable{
	private static final long serialVersionUID = -190125781453557206L;
	public String host;
	public int port;
	
	public RemoteObjectData( String host, int port ){
		this.host = host;
		this.port = port;
	}
}