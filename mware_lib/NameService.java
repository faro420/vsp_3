/**
 * 
 */
package mware_lib;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public abstract class NameService {
	public abstract void rebind( Object servant, String name );
	
	public abstract Object resolve( String name );
}
