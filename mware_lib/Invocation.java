/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha
 * @MatrNr 2141801
 *
 * 29.11.2016
 */
package mware_lib;

import java.io.Serializable;

public class Invocation<T extends Serializable, K extends Serializable> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private String fName;
	private String objName;

	private Primitiv<T> t;
	private Primitiv<K> k;

	public Invocation(String fName, String objName, T t, boolean bool1, K k,
			boolean bool2) {
		this.fName = fName;
		this.objName = objName;
		this.t = new Primitiv<T>(t, bool1);
		this.k = new Primitiv<K>(k, bool2);

	}

	public Primitiv<T> getT() {
		return t;
	}

	public Primitiv<K> getK() {
		return k;
	}

	public String getfName() {
		return fName;
	}

	public String getObjName() {
		return objName;
	}
}