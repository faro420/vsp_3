/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha
 * @MatrNr 2141801
 *
 * 29.11.2016
 */

package mware_lib;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

public class Service_Processor<T extends Serializable> extends Thread {
	private Object servant;
	private Socket skt;
	private static int instanceCount = 0;

	private Service_Processor(Socket skt, Object servant) {
		this.skt = skt;
		this.servant = servant;
	}

	public static Service_Processor getInstance(Socket skt, Object servant) {
		if (instanceCount < 4) {
			instanceCount++;
			return new Service_Processor(skt, servant);
		}
		return null;
	}

	public void run() {
		// hier kommt, was der Client haben möchte
		ObjectInputStream objectInput;
		Object a;
		try {
			objectInput = new ObjectInputStream(skt.getInputStream());
			// TODO Auto-generated catch block
			a = objectInput.readObject();
			Object o = invokeRequest(a);
			// hier kommt die Antwort
			ObjectOutputStream objectOutput = new ObjectOutputStream(skt.getOutputStream());
			objectOutput.writeObject(o);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		instanceCount--;
	}

	@SuppressWarnings("unchecked")
	private Object invokeRequest(Object request) {
		Object returnObj = null;

		Invocation<?, ?> invo = (Invocation<?, ?>) request;

		String oName = invo.getObjName();
		String fName = invo.getfName();

		int paramcount = (invo.getT().isBool() ? 1 : 0) + (invo.getK().isBool() ? 1 : 0);
		Primitiv<?> params[] = new Primitiv<?>[2];
		params[0] = invo.getT();
		params[1] = invo.getK();

		Class<?> c = servant.getClass();

		System.out.println("Invoke request for '" + fName + "' with " + paramcount + " parameters");

		Method[] allMethods = c.getDeclaredMethods();

		for (Method m : allMethods) {
			if (!m.getName().equals(fName))
				continue; // Method name doesn't match
			Class<?> p[] = m.getParameterTypes();
			if (p.length != paramcount)
				continue; // Parametercound doesn't match
			for (int i = 0; i < paramcount; i++)
				if (!p[i].getName().equals(params[i].t.getClass().getName()))
					continue; // Parameters don't match

			m.setAccessible(true);

			try {
				if (!m.getReturnType().equals(Void.TYPE)) {
					Object mre = null;
					switch (paramcount) {
					case 0:
						mre = m.invoke(servant);
						break;
					case 1:
						mre = m.invoke(servant, params[0].t);
						break;
					case 2:
						mre = m.invoke(servant, params[0].t, params[1].t);
						break;
					}
					returnObj = new Invocation_Return<>((T) mre);
				} else {
					returnObj = new Invocation_Return<>();
					switch (paramcount) {
					case 0:
						m.invoke(servant);
						break;
					case 1:
						m.invoke(servant, params[0].t);
						break;
					case 2:
						m.invoke(servant, params[0].t, params[1].t);
						break;
					}
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				returnObj = new Invocation_Return<>(e.getCause());
			}
		}

		return returnObj;
	}
}
