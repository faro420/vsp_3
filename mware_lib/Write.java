/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha (214 180 1)
 * @author Andreas M�ller (209 918 2)
 *
 * 29.11.2016
 */
package mware_lib;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Write {
	public static Object writeObject(Object obj, String hostIP, int port) {
		Object object = null;
		
		try {
			Socket socket = new Socket(hostIP, port);
			try {
				// Send Request
				ObjectOutputStream objectOutput = new ObjectOutputStream(socket.getOutputStream());
				objectOutput.writeObject(obj);
				
				// Wait for Response
				System.out.println("Local Port: " + socket.getLocalPort());
				ObjectInputStream objectInput = new ObjectInputStream(socket.getInputStream());
				try {
					object = objectInput.readObject();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return object;
	}
}
