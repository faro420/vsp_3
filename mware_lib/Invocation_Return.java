/**
 * 
 */
package mware_lib;

import java.io.Serializable;

/**
 * Project: VSP_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 29.11.2016
 */
public class Invocation_Return<T extends Serializable> implements Serializable {
	private static final long serialVersionUID = -1825677444062031204L;
	private T returnValue = null;
	private boolean isVoid = false;
	
	public Invocation_Return(){
		isVoid = true;
	}
	
	public Invocation_Return( T value ){
		this.returnValue = value;
	}
	
	public T getValue() {
		return returnValue;
	}
}
