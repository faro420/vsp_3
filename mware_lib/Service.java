/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha
 * @MatrNr 2141801
 *
 * 29.11.2016
 */
package mware_lib;

import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;

public class Service<T extends Serializable> implements Runnable {
	private ServiceInfo info;
	private Object servant;

	public Service(Object servant, ServiceInfo info) {
		// Create new ServiceInfo to avoid referencing the one from the
		// NameServiceImpl
		this.info = new ServiceInfo(info.getServiceName(), info.getServerIP(), info.getServicePort());
		this.servant = servant;
	}

	@SuppressWarnings("unused")
	public void run() {
		ServerSocket socket = null;
		@SuppressWarnings("rawtypes")
		Service_Processor thread = null;
		try {
			socket = new ServerSocket(info.getServicePort());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		while (!Thread.interrupted()) {

			System.out.println("Service: Accepting on Port " + info.getServicePort());
			try {
				Socket skt = socket.accept();
				thread = Service_Processor.getInstance(skt, servant);
				thread.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (Thread.interrupted()) {
				try {
					System.out.println("Service shutdown");
					socket.close();
					return;
				} catch (IOException e) {
					e.printStackTrace();
				}
				return;
			}
		}
	}

	public ServiceInfo getInfo() {
		return info;
	}
}
