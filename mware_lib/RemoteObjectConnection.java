/**
 * 
 */
package mware_lib;

/**
 * Project: VS_3
 *
 * @author Andreas M�ller
 * @MatrNr 209 918 2
 *
 * 26.11.2016
 */
public class RemoteObjectConnection{
	String host;
	int port;
	
	public RemoteObjectConnection( String host, int port ){
		this.host = host;
		this.port = port;
	}
}
