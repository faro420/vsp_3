/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha
 * @MatrNr 2141801
 *
 * 29.11.2016
 */
package mware_lib;

import java.io.Serializable;

public class ServiceInfo implements Serializable{
	private static final long serialVersionUID = 6965615948390597876L;
	private String serviceName;
	private String serverIP;
	private int servicePort;

	public ServiceInfo(String name, String IP, int port) {
		serviceName = name;
		serverIP = IP;
		servicePort = port;
	}

	public String getServiceName() {
		return serviceName;
	}

	public String getServerIP() {
		return serverIP;
	}

	public int getServicePort() {
		return servicePort;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public void setServicePort(int servicePort) {
		this.servicePort = servicePort;
	}

	public boolean equals(Object other) {
		ServiceInfo info = (ServiceInfo) other;
		return this.serviceName.equals(info.getServiceName()) && this.serverIP.equals(info.getServerIP())
				&& this.servicePort == info.getServicePort();
	}
}
