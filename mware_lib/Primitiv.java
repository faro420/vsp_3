/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha
 * @MatrNr 2141801
 *
 * 29.11.2016
 */
package mware_lib;

import java.io.Serializable;

public class Primitiv<T extends Serializable> implements Serializable {
	private static final long serialVersionUID = -7170849526983823657L;
	private boolean bool = false;
	T t;

	public Primitiv(T t, boolean bool) {
		this.t = t;
		this.bool = bool;
	}

	public boolean isBool() {
		return bool;
	}

	public T getT() {
		return t;
	}
}
