/**
 * Project: VSP_3
 *
 * @author Mir Farshid Baha (214 180 1)
 * @author Andreas M�ller (209 918 2)
 *
 * 29.11.2016
 */
package mware_lib;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class NameServiceImpl extends NameService {
	private ServiceInfo info; // Our Info: Our Address and Port
	private ServiceInfo dns;  // DNS info: DNS Address and Port
	
	private Server server;

	public NameServiceImpl(String hostIP, int port) {
		try {
			info = new ServiceInfo("", InetAddress.getLocalHost().getHostAddress(), 6666);
		} 
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
		dns = new ServiceInfo("", hostIP, port);
	}

	public void rebind(Object servant, String name) {
		info.setServiceName(name);
		Service_Anmeldung srvReg = new Service_Anmeldung( info );
		
		
		Object tmp = Write.writeObject(srvReg, dns.getServerIP(), dns.getServicePort());
		if( tmp.getClass().getSimpleName().equals( Service_Anmeldung.class.getSimpleName() )) {
			System.out.println("Object successfully rebinded!");
		}
		else {
			System.out.println("Object unsuccessfully rebinded!");
			return;
		}
		
		if( server == null ) server = new Server();
		
		server.addService( servant, info );
		info.setServicePort( info.getServicePort() + 1 );
	}

	public Object resolve(String name) {
		info.setServiceName( name );
		Service_Anfrage srvReq = new Service_Anfrage( info );
		
		Object tmp = Write.writeObject( srvReq, dns.getServerIP(), dns.getServicePort() );
		ServiceInfo srvInfo = ((Service_Anfrage) tmp).getInfo();
		
		if( srvInfo.getServerIP().equals( "" ) ||
			srvInfo.getServicePort() == 0 ||
			srvInfo.getServiceName().equals( "" ) ) {
			System.out.println("DNS dosn't know this object :(");
			return null;
		}
		
		return ((Service_Anfrage)tmp).getInfo();
	}
	
	public Server getServer() {
		return server;
	}

}
